package com.example.springbootjpa;

import com.example.springbootjpa.com.gzp.dao.PaymentDao;
import com.example.springbootjpa.com.gzp.entity.Payment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@SpringBootTest
class SpringbootJpaApplicationTests {
    @Autowired
    private PaymentDao dao;
    @Test
    void contextLoads() {
        add();

    }


    @Test
    public void add(){
        System.out.println("添加方法");
        Payment p = new Payment();
        p.setSerial("!1333333");
        dao.save(p);
    }


    @Test
    public void add2(){
        System.out.println("添加方法");
        Payment p = new Payment();
        p.setId("4028f9817a1789ff01df0000");
        p.setSerial("66666666666666");
        dao.save(p);
    }
    @Test
    public void  findAll(){
        List<Payment> all = dao.findAll();
        for (Payment payment : all) {
            System.out.println(payment);
        }
    }
}
