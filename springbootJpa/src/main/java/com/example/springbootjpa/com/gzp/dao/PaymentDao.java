package com.example.springbootjpa.com.gzp.dao;

import com.example.springbootjpa.com.gzp.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Component;

public interface PaymentDao extends JpaRepository<Payment, String>, JpaSpecificationExecutor<Payment> {


}
